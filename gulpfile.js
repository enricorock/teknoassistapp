var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');
var gcm = require('node-gcm')

var paths = {
  sass: ['./scss/**/*.scss']
};

var device = {
  apiKey: 'AIzaSyAoa9RKfslk4bvT5QD2IHkcOV1e1ONIBGA',
  deviceId: 'APA91bEyrE0Qb-qCCViYcdkgClj43JvxopuxQKEn5IhqLpCbJ5XJP0EiZPzm8S-vbgeERlz4qrEzjfjgg3jm3X3-qfpJSX16M1WJvV6VrM4ZVy5qZcyhge-JLDGWpV_x7izZmdxfYget'
};

gulp.task('default', ['sass']);

gulp.task('push:android', function() {
  if (!device.apiKey || device.apiKey === true) {
    throw new Error('You must specify the android Api key, refer to the documentation');
  }
  if (!device.deviceId || device.deviceId === true) {
    throw new Error('You must specify the android ID, refer to the documentation');
  }

  console.log('apiKey', device.apiKey);
  console.log('deviceId', device.deviceId);

  var message = new gcm.Message({
    collapseKey: 'demo',
    delayWhileIdle: true,
    timeToLive: 3,
    data: {
      key1: 'message1',
      key2: 'message2'
    }
  });

  var sender = new gcm.Sender(device.apiKey);

  sender.send(message, (device.deviceId instanceof Array) ? device.deviceId : [device.deviceId], 5, function(err, result) {
    if (err) {
      console.error('Failed, status code', err);
    } else {
      console.log('Success', result);
    }
  });
});

gulp.task('sass', function(done) {
  gulp.src('./scss/ionic.app.scss')
    .pipe(sass())
    .on('error', sass.logError)
    .pipe(gulp.dest('./www/css/'))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./www/css/'))
    .on('end', done);
});

gulp.task('watch', function() {
  gulp.watch(paths.sass, ['sass']);
});

gulp.task('install', ['git-check'], function() {
  return bower.commands.install()
    .on('log', function(data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
});

gulp.task('git-check', function(done) {
  if (!sh.which('git')) {
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Git, the version control system, is required to download Ionic.',
      '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
      '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    );
    process.exit(1);
  }
  done();
});
