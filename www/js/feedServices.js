
angular.module('feedService', []).factory('api', function($http) {
	var ENDPOINT = 'http://www.teknoassist.it/api/';
	var service = {
		getRecentPosts: function(count) {
		  return $http.get(ENDPOINT + "get_recent_posts", {
		      params: {
		        "count": count
		      }
		   })
		},
		getPosts: function(count) {
		  return $http.get(ENDPOINT + "get_posts", {
		      params: {
		        "count": count
		      }
		   })
		}
	};

	return service;
});