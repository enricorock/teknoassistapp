angular.module('notification', []).factory('notification', function($http, $rootScope) {
  return function init () {
    var androidConfig, iosConfig, register;

    androidConfig = {
      "senderID": "1006647389919"
    };
    iosConfig = {
      "badge": true,
      "sound": true,
      "alert": true
    };

    register = function(os, token) {
      var baseUrl;
      baseUrl = 'http://teknoassist.it/pnfw';
      if (!baseUrl) {
        return $q.reject();
      }
      return $http({
        method: 'POST',
        url: baseUrl + '/register',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        transformRequest: function(obj) {
          var p, str;
          str = [];
          for (p in obj) {
            str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
          }
          return str.join('&');
        },
        data: {
          os: os,
          token: token
        }
      });
    };

    register($rootScope.os, $rootScope.token);
  }
});
