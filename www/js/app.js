// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('starter', ['ionic', 'firebase', 'angularMoment', 'starter.controllers', 'starter.services', 'feedService', 'notification']);

app.run(function($ionicPlatform, $rootScope, $state, notification, Chats, Auth) {
  if ($ionicPlatform.is('Android')) {
    $rootScope.os = 'Android';
  } else if ($ionicPlatform.is('IOS')) {
    $rootScope.os = 'iOS';
  }
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs).
    // The reason we default this to hidden is that native apps don't usually show an accessory bar, at
    // least on iOS. It's a dead giveaway that an app is using a Web View. However, it's sometimes
    // useful especially with forms, though we would prefer giving the user a little more room
    // to interact with the app.
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      // Set the statusbar to use the default style, tweak this to
      // remove the status bar on iOS or change it to use white instead of dark colors.
      StatusBar.styleDefault();
    }

    Chats.authenticate();
    $rootScope.logout = function () {
        Chats.logout();
    }

    $rootScope.$on("$stateChangeError", function (event, toState, toParams, fromState, fromParams, error) {
        // We can catch the error thrown when the $requireAuth promise is rejected
        // and redirect the user back to the home page
        if (error === "AUTH_REQUIRED") {
            $state.go("app.login");
        }
    });


    // Android Push

    var io = Ionic.io();
    var push = new Ionic.Push({
      'onNotification': function(notification) {
        alert('Received Notification!');
      },
      'pluginConfig': {
        'android': {
          'iconColor': '#0000FF'
        }
      },
      'onRegister': function(data) {
        $rootScope.token = data.token;
        notification();
      },
    });

    var user = Ionic.User.current();

    if (!user.id) {
      user.id = Ionic.User.anonymousId();
    }


    user.set('name', 'Simeon');
    user.set('bio', 'this is bio');
    user.save();

    var callback = function() {
      var i = push.addTokenToUser(user);
      console.log(i);
      user.save();
    };

    push.register(callback);
  });
});

app.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
  $ionicConfigProvider.views.transition('android');
  $ionicConfigProvider.tabs.position('bottom')
    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
  $stateProvider
    .state('app', {
      url: "/app",
      abstract: true,
      templateUrl: "templates/menu.html",
      controller: 'MenuCtrl'
    })
    // setup an abstract state for the tabs directive
    .state('app.home', {
      url: '/home',
      views: {
        'content': {
          templateUrl: 'templates/home.html',
          controller: 'HomeCtrl'
        }
      }
    })
    // news state
    .state('app.news', {
      url: '/news',
      views: {
        'content': {
          templateUrl: 'templates/news.html',
          controller: 'NewsCtrl'
        }
      }
    })

  // State to represent Login View
  .state('app.login', {
    url: "/login",
    views: {
      'content': {
        templateUrl: "templates/login.html",
        controller: 'LoginCtrl',
        resolve: {
          // controller will not be loaded until $waitForAuth resolves
          // Auth refers to our $firebaseAuth wrapper in the example above
          "currentAuth": ["Auth",
            function(Auth) {
              // $waitForAuth returns a promise so the resolve waits for it to complete
              return Auth.$waitForAuth();
            }
          ]
        }
      }
    }
  })

  // setup an abstract state for the tabs directive
  .state('app.chat', {
    url: "/chat",
    views: {
      'content': {
        abstract: true,
        templateUrl: "templates/tabs-home.html",
        resolve: {
          // controller will not be loaded until $requireAuth resolves
          // Auth refers to our $firebaseAuth wrapper in the example above
          "currentAuth": ["Auth",
            function(Auth) {
              // $requireAuth returns a promise so the resolve waits for it to complete
              // If the promise is rejected, it will throw a $stateChangeError (see above)
              return Auth.$requireAuth();
            }
          ]
        }
      }
    }
  })

  // Each tab has its own nav history stack:

  .state('app.chat.rooms', {
    url: '/rooms',
    views: {
      'chat-rooms': {
        templateUrl: 'templates/tab-rooms.html',
        controller: 'RoomsCtrl'
      }
    }
  })

  .state('app.chat.home', {
    url: '/:roomId',
    views: {
      'chat-home': {
        templateUrl: 'templates/tab-chat.html',
        controller: 'ChatCtrl'
      }
    }
  })

  // $urlRouterProvider.otherwise('app/home');

});
