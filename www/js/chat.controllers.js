angular.module('starter').controller('LoginCtrl', function($scope, Chats, $ionicModal) {
  //console.log('Login Controller Initialized');

  $ionicModal.fromTemplateUrl('templates/signup.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.createUser = function(user) {
    Chats.createUser(user);
  };

  $scope.signIn = function(user) {
    Chats.signIn(user);
  };
})

.controller('ChatCtrl', function($scope, Chats, $state, $ionicScrollDelegate, $timeout, $rootScope) {

  $scope.IM = {
    textMessage: ""
  };
  $rootScope.$watch('from', function(val) {
    if (val) {
      $scope.displayName = $rootScope.from.displayName;
    }
  });

  Chats.selectRoom($state.params.roomId);

  var roomName = Chats.getSelectedRoomName();

  // Fetching Chat Records only if a Room is Selected
  if (roomName) {
    $scope.roomName = " - " + roomName;
    $scope.chats = Chats.all();
    $timeout(function() {
      $ionicScrollDelegate.$getByHandle('chatScroll').scrollBottom(true);
    });
    $scope.chats.$watch(function(val) {
      $ionicScrollDelegate.$getByHandle('chatScroll').scrollBottom(true);
    });
  }
  $scope.sendMessage = function(msg) {
    console.log(msg);
    Chats.send($scope.displayName, msg);
    $scope.IM.textMessage = "";
  };

  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('RoomsCtrl', function($scope, Rooms, Chats, $state, PresenceService) {
  //console.log("Rooms Controller initialized");
  $scope.rooms = Rooms.all();
  $scope.rooms.$loaded().then(function () {
    PresenceService.getRoomOnline($scope.rooms);
  });
  $scope.rooms.$watch(function() {
    PresenceService.getRoomOnline($scope.rooms);
  });



  $scope.openChatRoom = function(roomId) {
    $state.go('app.chat.home', {
      roomId: roomId
    });
  };
});
