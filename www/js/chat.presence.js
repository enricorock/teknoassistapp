angular.module('starter').factory('PresenceService', ['$rootScope', '$firebaseObject', '$timeout', function($rootScope, $firebaseObject, $timeout) {
  var firebaseUrl = config.url;
  var onlineUsers = 0;
  var listRef = new Firebase(firebaseUrl + '/presence/');
  var adminListRef = new Firebase(firebaseUrl + '/admin/');
  var userRef = listRef.push();
  var detectRooms;

  var adminOnline = function() {
    // Add ourselves to presence list when online.
    if ($rootScope.admin) {
      var adminPresenceRef = new Firebase(firebaseUrl + '/.info/connected');
      adminPresenceRef.on('value', function(snap) {
        if (snap.val()) {
          adminListRef.set(true);
          // Remove admin when disconnect.
          adminListRef.onDisconnect().remove();
        }
      });
    } else {
      adminListRef.on("value", function(snapshot) {
        $rootScope.$apply(function() {
          $rootScope.adminOnline = snapshot.val();
        });
      });
    }
  }

  // Add ourselves to presence list when online.
  var presenceRef = new Firebase(firebaseUrl + '/.info/connected');
  presenceRef.on('value', function(snap) {
    adminOnline();
    if (snap.val()) {
      userRef.set(true, function() {
        $rootScope.onlineId = userRef.key();
      });
      // $rootScope.onlineId =
      // Remove admin when disconnect.
      userRef.onDisconnect().remove();
    }
  });

  listRef.on('child_changed', function () {
  	$timeout(function () {
  		getRoomOnline(detectRooms);
  	});
  });

  // Number of online users is the number of objects in the presence list.
  listRef.on('value', function(snap) {
    onlineUsers = snap.numChildren();
    $rootScope.$broadcast('onOnlineUser');
    console.log(onlineUsers);
  });

  var getOnlineUserCount = function() {
    return onlineUsers;
  };

  var getRoomOnline = function(rooms) {
    detectRooms = rooms;

    var triggerList = function () {
    	$timeout(function () {
    		$firebaseObject(listRef).$loaded().then(function (snap) {
    			rooms.forEach(function(room) {
    			  if (room.user) {
    			    if (snap[room.user]) {
    			      room.online = true;
    			    } else {
    			      room.online = false;
    			    }
    			  }
    			});
    		});
    	});
    }

    $firebaseObject(listRef).$watch(triggerList);
  };

  return {
    getOnlineUserCount: getOnlineUserCount,
    adminOnline: adminOnline,
    getRoomOnline: getRoomOnline
  }
}]);
