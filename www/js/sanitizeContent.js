angular.module('starter').filter('sanitizeContent', [function() {
  return function(text) {
    return angular.element(text).text();
  }
}]);
