
var config = config || {};
config.url = 'https://teknoassist.firebaseio.com';
angular.module('starter')
  .factory("Auth", ["$firebaseAuth", "$rootScope",
    function($firebaseAuth, $rootScope) {
      var ref = new Firebase(config.url);
      return $firebaseAuth(ref);
    }
  ])
  .factory('Chats',
    function($firebaseArray, $firebaseAuth, Auth, Rooms, PresenceService,$ionicScrollDelegate, $rootScope, $ionicModal, $ionicLoading, $location, PresenceService, $state, $timeout, $stateParams) {
      var selectedRoomId;
      var ref = new Firebase(config.url);
      var auth = $firebaseAuth(ref);
      var chats;
      var chatRef;

      return {
        all: function() {
          return chats;
        },
        autObj: function() {
          return auth;
        },
        authenticate: function() {
          // quite same as services
          Auth.$onAuth(function(authData) {
            if (authData) {
              $rootScope.currentUser = authData.uid;
              ref.child("users").child(authData.uid).once('value', function(snapshot) {
                console.dir($stateParams);
                // To Update AngularJS $scope either use $apply or $timeout
                if (snapshot.val().email === 'info@teknoassist.it') {
                  $rootScope.admin = true;
                  if ($stateParams.roomId) {
                    $location.path('/app/chat/rooms');
                  }
                }
                var val = snapshot.val();
                $timeout(function() {
                  $rootScope.from = val;
                  PresenceService.adminOnline();
                  if (!$rootScope.admin && $stateParams.roomId) {
                    $location.path('/app/chat/' + authData.uid);
                  }
                });
              });
            } else {
              console.log("Logged out");
              $rootScope.currentUser = '';
              setTimeout(function() {
                $ionicLoading.hide();
              }, 600);
              $location.path('/app/home');
            }
          });
        },
        createUser: function(user) {
          console.log("Create User Function called");
          if (user && user.email && user.password && user.displayname) {
            $ionicLoading.show({
              template: 'Signing Up...'
            });

            Auth.$createUser({
              email: user.email,
              password: user.password
            }).then(function(userData) {
              alert("User created successfully!");
              ref.child("users").child(userData.uid).set({
                email: user.email,
                displayName: user.displayname
              });
              $ionicLoading.hide();
              $rootScope.modal.hide();
            }).catch(function(error) {
              alert("Error: " + error);
              $ionicLoading.hide();
            });
          } else {
            alert("Please fill all details");
          }
        },
        signIn: function(user) {
          if (user && user.email && user.pwdForLogin) {
            $ionicLoading.show({
              template: 'Signing In...'
            });
            Auth.$authWithPassword({
              email: user.email,
              password: user.pwdForLogin
            }).then(function(authData) {
              console.log("Logged in as:" + authData.uid);
              ref.child("users").child(authData.uid).once('value', function(snapshot) {
                // To Update AngularJS $scope either use $apply or $timeout
                var val = snapshot.val();
                if (snapshot.val().email === 'info@teknoassist.it') {
                  $rootScope.admin = true;
                  if ($stateParams.roomId) {
                    $location.path('/app/chat/rooms');
                  }
                }
                $timeout(function() {
                  $rootScope.from = val;
                  if (!$rootScope.admin && $stateParams.roomId) {
                    $location.path('/app/chat/' + authData.uid);
                  }
                });
              });
              $ionicLoading.hide();
            }).catch(function(error) {
              alert("Authentication failed:" + error.message);
              $ionicLoading.hide();
            });
          } else {
            alert("Please enter email and password both");
          }
        },
        logout: function() {
          console.log("Logging out from the app");
          $ionicLoading.show({
            template: 'Logging Out...'
          }, 600);
          Auth.$unauth();
        },
        remove: function(chat) {
          chats.$remove(chat).then(function(ref) {
            ref.key() === chat.$id; // true item has been removed
          });
        },
        get: function(chatId) {
          for (var i = 0; i < chats.length; i++) {
            if (chats[i].id === parseInt(chatId)) {
              return chats[i];
            }
          }
          return null;
        },
        getSelectedRoomName: function() {
          var selectedRoom;
          if (selectedRoomId && selectedRoomId != null) {
            return selectedRoomId;
          } else {
            return null;
          }
        },
        selectRoom: function(roomId) {
          console.log("selecting the room with id: " + roomId);
          selectedRoomId = roomId;
          chatRef = ref.child('rooms').child(selectedRoomId);
          chats = $firebaseArray(chatRef.child('chats'));
          if (!$rootScope.admin) {
            if ($rootScope.from) {
              ref.child('rooms').child(selectedRoomId).update({
                email: $rootScope.from.email,
                name: $rootScope.from.displayName,
                id: selectedRoomId
              });
            } else {
              $rootScope.$watch('from', function(val) {
                if (val) {
                  ref.child('rooms').child(selectedRoomId).update({
                    email: $rootScope.from.email,
                    name: $rootScope.from.displayName,
                    id: selectedRoomId
                  });
                }
              });
            }
          }
          console.log(chats);
        },
        send: function(from, message) {
          console.log("sending message from :" + from + " & message is " + message);
          if (from && message) {
            var chatMessage = {
              from: from,
              message: message,
              createdAt: Firebase.ServerValue.TIMESTAMP,
              lastMessage: Firebase.ServerValue.TIMESTAMP,
              user: $rootScope.onlineId
            };
            if (!$rootScope.admin) {
              chatRef.update({
                lastMessage: Firebase.ServerValue.TIMESTAMP,
                user: $rootScope.onlineId
              });
            }
            chats.$add(chatMessage).then(function(data) {
              $ionicScrollDelegate.$getByHandle('chatScroll').scrollBottom(true);
            });
          }
        }
      };
    })

/**
 * Simple Service which returns Rooms collection as Array from Salesforce & binds to the Scope in Controller
 */
.factory('Rooms', function($firebaseArray, $q, PresenceService) {
  // Might use a resource here that returns a JSON array
  var ref = new Firebase(config.url);
  var rooms = $firebaseArray(ref.child('rooms'));
  var listRef = new Firebase(config.url + '/presence/');


  return {
    all: function() {
      return rooms;
    }
  };
});
