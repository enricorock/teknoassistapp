angular.module('starter').filter('limitContent', [function() {
  return function(text, limit) {
    return text.substring(0, limit) + '...';
  }
}]);