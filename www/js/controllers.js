angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope) {})

.controller('ChatDetailCtrl', function($scope) {})

.controller('AccountCtrl', function($scope) {})

.controller('HomeCtrl', function($scope, $ionicSlideBoxDelegate, api, $rootScope) {
	if (!$scope.slideNews) {
		api.getRecentPosts(3).then(function (res) {
			$scope.slideNews = res.data.posts;
		})
	}
	$scope.openCordovaWebView = function(url, entity, options) {
		 // Open cordova webview if the url is in the whitelist otherwise opens in app browser
		 window.open(url, entity, options);
	};

	// $rootScope.$on('$cordovaPush:tokenReceived', function (event) {
	// 	alert('Success:' + data.token);
	// 	console.log('Got token:'. data.token, data.platform);
	// 	$scope.token = data.token;
	// });

	// $scope.identifyUser = function () {
	// 	var user = $ionicUser.get();

	// 	if (!user.user_id) {
	// 		user.user_id = $ionicUser.generateGUID();
	// 	}

	// 	angular.extend(user, {
	// 		name: 'Mario',
	// 		bio: 'Daje'
	// 	});

	// 	$ionicUser.identify(user).then(function () {
	// 		$scope.identified = true;
	// 		console.log('name:' + user.name + ' ---id: ' + user.id);
	// 	});

	// 	$scope.pushRegister = function () {
	// 		$ionicPush.register({
	// 			canShowAlert: true,
	// 			canSetBadge: true,
	// 			canPlaySound: true,
	// 			canRunActionsOnWake: true,
	// 			onNotification: function (notification) {
	// 				// handle your stuff
	// 				return true;
	// 			}
	// 		});
	// 	};
	// };
})

.controller('NewsCtrl', function($scope, $ionicSlideBoxDelegate, api) {
	if (!$scope.newsList) {
		api.getPosts(10).then(function (res) {
			$scope.newsList = res.data.posts;
		})
	}
	$scope.openCordovaWebView = function(url, entity, options) {
		 // Open cordova webview if the url is in the whitelist otherwise opens in app browser
		 window.open(url, entity, options);
	};
})

.controller('MenuCtrl', function($scope) {
	$scope.openCordovaWebView = function(url, entity, options) {
		 // Open cordova webview if the url is in the whitelist otherwise opens in app browser
		 window.open(url, entity, options);
	};
});
